/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/utils/RateLimit.coffee ---- */

(function () {
  var callAfterInterval, limits;

  limits = {};

  callAfterInterval = {};

  window.RateLimit = function (interval, fn) {
    if (!limits[fn]) {
      callAfterInterval[fn] = false;
      fn();
      return (limits[fn] = setTimeout(function () {
        if (callAfterInterval[fn]) {
          fn();
        }
        delete limits[fn];
        return delete callAfterInterval[fn];
      }, interval));
    } else {
      callAfterInterval[fn] = true;
      return callAfterInterval[fn];
    }
  };
}).call(this);
