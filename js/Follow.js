/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/utils/Follow.coffee ---- */

(function () {
  var Follow;
  var bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };
  var extend = function (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) child[key] = parent[key];
    }
    function Ctor () {
      this.constructor = child;
    }
    Ctor.prototype = parent.prototype;
    child.prototype = new Ctor();
    child.__super__ = parent.prototype;
    return child;
  };
  var hasProp = {}.hasOwnProperty;
  var indexOf = [].indexOf || function (item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
  /**
   * @class {Follow} Follow
   * @description Handles subscribing to content in ZeroNets newsfeed
   */
  Follow = (function (superClass) {
    extend(Follow, superClass);

    function Follow (elem) {
      this.elem = elem;
      this.handleMenuClick = bind(this.handleMenuClick, this);
      this.init = bind(this.init, this);
      this.menu = new Menu(this.elem);
      this.feeds = {};
      this.follows = {};
      this.elem.on('click', (function (_this) {
        return function () {
          if (Page.server_info.rev > 850) {
            if (_this.elem.hasClass('following')) {
              _this.showFeeds();
            } else {
              _this.followDefaultFeeds();
            }
          } else {
            Page.cmd('wrapperNotification', ['info', 'Please update your ZeroNet client to use this feature']);
          }
          return false;
        };
      })(this));
    }

    Follow.prototype.init = function () {
      if (!this.feeds) {
        return;
      }
      return Page.cmd('feedListFollow', [], (function (_this) {
        return function (follows1) {
          /*eslint-disable no-unused-vars*/var isDefaultFeed, menuItem, param, query, ref, ref1, title;/* eslint-enable*/
          _this.follows = follows1;
          ref = _this.feeds;
          for (title in ref) {
            ref1 = ref[title];
            query = ref1[0];
            menuItem = ref1[1];
            isDefaultFeed = ref1[2];
            param = ref1[3];
            if (_this.follows[title] && indexOf.call(_this.follows[title][1], param) >= 0) {
              menuItem.addClass('selected');
            } else {
              menuItem.removeClass('selected');
            }
          }
          _this.updateListitems();
          return _this.elem.css('display', 'inline-block');
        };
      })(this));
    };

    Follow.prototype.addFeed = function (title, query, isDefaultFeed, param) {
      var menuItem;
      if (isDefaultFeed == null) {
        isDefaultFeed = false;
      }
      if (param == null) {
        param = '';
      }
      menuItem = this.menu.addItem(title, this.handleMenuClick);
      this.feeds[title] = [query, menuItem, isDefaultFeed, param];
      return this.feeds[title];
    };

    Follow.prototype.handleMenuClick = function (item) {
      item.toggleClass('selected');
      this.updateListitems();
      this.saveFeeds();
      return true;
    };

    Follow.prototype.showFeeds = function () {
      return this.menu.show();
    };

    Follow.prototype.followDefaultFeeds = function () {
      /*eslint-disable no-unused-vars*/var isDefaultFeed, menuItem, param, query, ref, ref1, title;/* eslint-enable*/
      ref = this.feeds;
      for (title in ref) {
        ref1 = ref[title];
        query = ref1[0];
        menuItem = ref1[1];
        isDefaultFeed = ref1[2];
        param = ref1[3];
        if (isDefaultFeed) {
          menuItem.addClass('selected');
          this.log('Following', title);
        }
      }
      this.updateListitems();
      return this.saveFeeds();
    };

    Follow.prototype.updateListitems = function () {
      if (this.menu.elem.find('.selected').length > 0) {
        return this.elem.addClass('following');
      } else {
        return this.elem.removeClass('following');
      }
    };

    Follow.prototype.saveFeeds = function () {
      return Page.cmd('feedListFollow', [], (function (_this) {
        return function (follows) {
          /*eslint-disable no-unused-vars*/var isDefaultFeed, item, menuItem, param, params, query, ref, ref1, title;/* eslint-enable*/
          _this.follows = follows;
          ref = _this.feeds;
          for (title in ref) {
            ref1 = ref[title];
            query = ref1[0];
            menuItem = ref1[1];
            isDefaultFeed = ref1[2];
            param = ref1[3];
            if (follows[title]) {
              params = (function () {
                var i, len, ref2, results;
                ref2 = follows[title][1];
                results = [];
                for (i = 0, len = ref2.length; i < len; i++) {
                  item = ref2[i];
                  if (item !== param) {
                    results.push(item);
                  }
                }
                return results;
              })();
            } else {
              params = [];
            }
            if (menuItem.hasClass('selected')) {
              params.push(param);
            }
            if (params.length === 0) {
              delete follows[title];
            } else {
              follows[title] = [query, params];
            }
          }
          return Page.cmd('feedFollow', [follows]);
        };
      })(this));
    };

    return Follow;
  })(Class);

  window.Follow = Follow;
}).call(this);
