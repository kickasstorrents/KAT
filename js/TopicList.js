/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/TopicList.coffee ---- */

(function () {
  var TopicList;
  var bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };
  var extend = function (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) child[key] = parent[key];
    }
    function Ctor () {
      this.constructor = child;
    }
    Ctor.prototype = parent.prototype;
    child.prototype = new Ctor();
    child.__super__ = parent.prototype;
    return child;
  };
  var hasProp = {}.hasOwnProperty;

  TopicList = (function (superClass) {
    extend(TopicList, superClass);

    function TopicList () {
      this.submitTopicVote = bind(this.submitTopicVote, this);
      this.thread_sorter = null;
      this.parent_topic_uri = void 0;
      this.list_all = false;
      this.topic_parent_uris = {};
      this.topic_sticky_uris = {};
    }

    TopicList.prototype.actionList = function (parentTopicId, parentTopicUserAddress, category) {
      var i, len, ref, topicStickyUri;
      if (category == null) {
        category = false;
      }
      ref = Page.site_info.content.settings.topic_sticky_uris;
      for (i = 0, len = ref.length; i < len; i++) {
        topicStickyUri = ref[i];
        this.topic_sticky_uris[topicStickyUri] = 1;
      }
      $('.topics-loading').cssLater('top', '0px', 200);
      if (parentTopicId) {
        $('.topics-title').html('&nbsp;');
        this.parent_topic_uri = parentTopicId + '_' + parentTopicUserAddress;
        Page.local_storage['topic.' + parentTopicId + '_' + parentTopicUserAddress + '.visited'] = Time.timestamp();
        Page.cmd('wrapperSetLocalStorage', Page.local_storage);
        this.loadTopics('noanim');
      } else if (category !== false) {
        $('.topics-title').html(category);
        this.loadTopics('noanim', false, false, category);
      } else {
        /*
            To Do:
                * I had a problem so I decided to use regular expressions. Now I have two problems...
        */
        var search = window.location.href.match(/(\??|&?)q=(.*)/);
        if (search) {
          search = decodeURIComponent(search.pop().replace(/&Category:(.*)/g, '').replace(/&wrapper_nonce=(.*)/g, ''));
          $('.topics-title').html(search);
          this.loadTopics('list', false, search, $('#torrent_search_category').val());
        } else {
            // To Do:
            //  * Add torrents to table using SQL below
            //  * Update table when new torrents are uploaded
            // SELECT * FROM topic WHERE category LIKE 'music' ORDER BY added DESC LIMIT 15
          $('.topics-title').html('Newest torrents');
          this.loadTopics('noanim');
          [
            {
              category: 'movies',
              div: 'latestMovies',
              iconType: 'filmType'
            },
            {
              category: 'tv',
              div: 'latestTv',
              iconType: 'filmType'
            },
            {
              category: 'music',
              div: 'latestMusic',
              iconType: 'musicType'
            },
            {
              category: 'games',
              div: 'latestGames',
              iconType: 'exeType'
            },
            {
              category: 'applications',
              div: 'latestApplications',
              iconType: 'zipType'
            },
            {
              category: 'anime',
              div: 'latestAnime',
              iconType: 'filmType'
            },
            {
              category: 'books',
              div: 'latestBooks',
              iconType: 'pdfType'
            },
            {
                    /*
                        To Do:
                            Add support for sub-categories to make querying for things like lossless audio much easier
                            than the below query.
                    */
              category: "music' AND title LIKE '%flac%' OR title LIKE '%aif%' OR title LIKE '%wav%",
              div: 'latestLosslessMusic',
              iconType: 'musicType'
            }
          ].map(function (el) {
            Page.cmd('dbQuery', [
              "SELECT * FROM topic WHERE category LIKE '" + el.category + "' ORDER BY added DESC LIMIT 15"
            ], function (res) {
                  // console.dir(res);
              var latestThing = (res.map(function (res) {
                return {
                  title: res.title,
                  comments: 0,
                  files: 0,
                  age: res.added,
                  seeders: res.seeders || 0,
                  leechers: res.leechers || 0,
                  permalink: '?full&Topic:' + res.topic_id + '_' + res.torrent_file.split('/')[2]
                };
              }));
              var row = document.createElement('tr');
              for (var i = latestThing.length - 1; i > -1; i--) {
                row.className = !(i % 2) ? 'odd' : 'even';
                row.innerHTML = '<td>' +
                          '<div class="iaconbox floatright">' +
                          ((latestThing[i].comments > 0) ? (
                              '<a class="icommentjs kaButton smallButton rightButton" href="' + latestThing[i].permalink + '#comment">' +
                              latestThing[i].comment +
                              ' <i class="ka ka-comment"></i>' +
                              '</a>')
                          : '') + '</div>' +
                          '<div class="markeredBlock torType ' + el.iconType + '">' +
                              '<a href="' + latestThing[i].permalink + '" class="cellMainLink">' + latestThing[i].title + '</a>' +
                          '</div>' +
                      '</td>' +
                      '<td class="nobr center">&nbsp;<!-- FILE SIZE HERE --><span><!-- FILE SIZE UNIT HERE e.g GB --></span></td>' +
                      '<td class="center"><!-- NUMBER OF FILES GOES HERE --></td>' +
                      '<td class="center"><span class="timeago">' + moment(latestThing[i].age * 1000).format('D MMM YYYY, hh:mm:ss') + '</span></td>' +
                      '<td class="green center">' + latestThing[i].seeders + '</td>' +
                      '<td class="red lasttd center">' + latestThing[i].leechers + '</td>';
                document.getElementById(el.div).getElementsByClassName('firstr')[0].parentNode.insertBefore(row, document.getElementById(el.div).getElementsByClassName('firstr')[0].nextSibling);
                row = document.createElement('tr');
              }
            }); });
        }
      }
      $('.topic-new-link').on('click', (function (_this) {
        return function () {
          $('.topic-new').fancySlideDown();
          $('.topic-new-link').slideUp();
          return false;
        };
      })(this));
      $('.topic-new .button-submit').on('click', (function (_this) {
        return function () {
          _this.submitCreateTopic();
          return false;
        };
      })(this));
      $('#contentSearch').on('keypress', (function (_this) {
        return function (e) {
          if (e.keyCode === 13) {
            _this.loadTopics('list', false, e.currentTarget.value, $('#torrent_search_category').val());
            $('#originalZeroTorrent').fadeIn();
            setTimeout(function () {
              $('html,body').animate({scrollTop: $('.topics-list').offset().top}, 'normal');
            }, 100);
            return false;
          }
        };
      })(this));
      $('#tagcloud a').on('click', (function (_this) {
        return function (e) {
          e.preventDefault();
          _this.loadTopics('list', false, e.currentTarget.textContent, $('#torrent_search_category').val());
          $('#originalZeroTorrent').fadeIn();
          setTimeout(function () {
            $('html,body').animate({scrollTop: $('.topics-list').offset().top}, 'normal');
          }, 100);
          return false;
        };
      })(this));
      $('#latestSearches a').on('click', (function (_this) {
        return function (e) {
          e.preventDefault();
          _this.loadTopics('list', false, e.currentTarget.textContent, $('#torrent_search_category').val());
          $('#originalZeroTorrent').fadeIn();
          setTimeout(function () {
            $('html,body').animate({scrollTop: $('.topics-list').offset().top}, 'normal');
          }, 100);
          return false;
        };
      })(this));
      $('#torrent_search_category').on('change', (function (_this) {
        return function (e) {
          $('#originalZeroTorrent').fadeIn();
          _this.loadTopics('list', false, $('#torrent_search').val(), e.currentTarget.value);
          return false;
        };
      })(this));
      $('.topics-more').on('click', (function (_this) {
        return function () {
          _this.list_all = true;
          $('.topics-more').text('Loading...');
          _this.loadTopics('noanim');
          return false;
        };
      })(this));
      /*
        To Do:
          * Remove me after proper support for categories has been added.
      */
      if (window.location.href.match(/(\??|&)Category:/)) {
        $('#originalZeroTorrent').fadeIn();
        setTimeout(function () {
          $('html,body').animate({scrollTop: $('.topics-list').offset().top}, 'normal');
        }, 1000);
      }
      return this.initFollowButton();
    };

    TopicList.prototype.initFollowButton = function () {
      var username;
      this.follow = new Follow($('.feed-follow-list'));
      if (this.parent_topic_uri) {
        this.follow.addFeed('New topics in this group', "SELECT title AS title, body, added AS date_added, 'topic' AS type, '?full&Topic:' || topic.topic_id || '_' || topic_creator_json.directory AS url, parent_topic_uri AS param FROM topic LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id) WHERE parent_topic_uri IN (:params)", true, this.parent_topic_uri);
      } else {
        this.follow.addFeed('New topics', "SELECT title AS title, body, added AS date_added, 'topic' AS type, '?full&Topic:' || topic.topic_id || '_' || topic_creator_json.directory AS url FROM topic LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id) WHERE parent_topic_uri IS NULL", true);
        if (Page.site_info.cert_user_id) {
          username = Page.site_info.cert_user_id.replace(/@.*/, '');
          this.follow.addFeed('Username mentions', "SELECT 'mention' AS type, comment.added AS date_added, topic.title, commenter_user.value || ': ' || comment.body AS body, topic_creator_json.directory AS topic_creator_address, topic.topic_id || '_' || topic_creator_json.directory AS row_topic_uri, '?full&Topic:' || topic.topic_id || '_' || topic_creator_json.directory AS url FROM topic LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id) LEFT JOIN comment ON (comment.topic_uri = row_topic_uri) LEFT JOIN json AS commenter_json ON (commenter_json.json_id = comment.json_id) LEFT JOIN json AS commenter_content ON (commenter_content.directory = commenter_json.directory AND commenter_content.file_name = 'content.json') LEFT JOIN keyvalue AS commenter_user ON (commenter_user.json_id = commenter_content.json_id AND commenter_user.key = 'cert_user_id') WHERE comment.body LIKE '%[" + username + "%' OR comment.body LIKE '%@" + username + "%'", true);
        }
        this.follow.addFeed('All comments', "SELECT 'comment' AS type, comment.added AS date_added, topic.title, commenter_user.value || ': ' || comment.body AS body, topic_creator_json.directory AS topic_creator_address, topic.topic_id || '_' || topic_creator_json.directory AS row_topic_uri, '?full&Topic:' || topic.topic_id || '_' || topic_creator_json.directory AS url FROM topic LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id) LEFT JOIN comment ON (comment.topic_uri = row_topic_uri) LEFT JOIN json AS commenter_json ON (commenter_json.json_id = comment.json_id) LEFT JOIN json AS commenter_content ON (commenter_content.directory = commenter_json.directory AND commenter_content.file_name = 'content.json') LEFT JOIN keyvalue AS commenter_user ON (commenter_user.json_id = commenter_content.json_id AND commenter_user.key = 'cert_user_id')");
      }
      return this.follow.init();
    };

    /*
        To Do:
            * Load torrents on main page - 15 for each category
    */

    TopicList.prototype.loadTopics = function (type, cb, search, category) {
      var elem, elems, i, lastElem, len, query, where, whereb;
      if (type == null) {
        type = 'list';
      }
      if (cb == null) {
        cb = false;
      }
      if (search == null) {
        search = false;
      }
      if (category == null) {
        category = false;
      }
      this.logStart('Load topics...');
      if (this.parent_topic_uri) {
        where = "WHERE parent_topic_uri = '" + this.parent_topic_uri + "' OR row_topic_uri = '" + this.parent_topic_uri + "'";
      } else {
        where = 'WHERE topic.type IS NULL AND topic.parent_topic_uri IS NULL AND (comment.added < ' + (Date.now() / 1000 + 120) + ' OR comment.added IS NULL)';
      }
      if (category === false || category === 'all') {
        whereb = '';
      } else {
        whereb = ' AND topic.category LIKE "' + category + '"';
        if (where) {
          where = where + ' AND topic.category LIKE "' + category + '"';
        } else {
          where = 'WHERE topic.category LIKE "' + category + '"';
        }
      }
      if (search !== false) {
        whereb = whereb + ' AND topic.title LIKE "%' + search + '%"';
        if (where) {
          where = where + ' AND topic.title LIKE "%' + search + '%"';
        } else {
          where = 'WHERE topic.title LIKE "%' + search + '%"';
        }
        elems = $('.topic[id^=topic]');
        for (i = 0, len = elems.length; i < len; i++) {
          elem = elems[i];
          elem.parentNode.removeChild(elem);
        }
      } else {
        whereb = '';
      }
      lastElem = $('.topics-list .topic.template');
      /*
        To Do:
            * Find a better way of writing / reading SQL - The original ZeroTorrent CoffeScript probably wasn't all on one line.
      */
      /*
        SELECT
        COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, topic.added as last_added, CASE WHEN MAX(comment.added) IS NULL THEN topic.added ELSE MAX(comment.added) END as last_action,
        topic.*,
        topic_creator_user.value AS topic_creator_user_name,
        topic_creator_content.directory AS topic_creator_address,
        topic.topic_id || _ || topic_creator_content.directory AS row_topic_uri,
        NULL AS row_topic_sub_uri,
        (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || _ || topic_creator_content.directory)+1 AS votes
        FROM topic
        LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)
        LEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = content.json)
        LEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = cert_user_id)
        LEFT JOIN comment ON (comment.topic_uri = row_topic_uri)
        " + where + "
        GROUP BY topic.topic_id, topic.json_id
      */
      query = "SELECT\n COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, topic.added as last_added, CASE WHEN MAX(comment.added) IS NULL THEN topic.added ELSE MAX(comment.added) END as last_action,\n topic.*,\n topic_creator_user.value AS topic_creator_user_name,\n topic_creator_content.directory AS topic_creator_address,\n topic.topic_id || '_' || topic_creator_content.directory AS row_topic_uri,\n NULL AS row_topic_sub_uri,\n (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || '_' || topic_creator_content.directory)+1 AS votes\nFROM topic\nLEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)\nLEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = 'content.json')\nLEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = 'cert_user_id')\nLEFT JOIN comment ON (comment.topic_uri = row_topic_uri)\n" + where + '\nGROUP BY topic.topic_id, topic.json_id';
      if (!this.parent_topic_uri) {
        /*
            To Do:
                * Find a better way of writing / reading SQL - The original ZeroTorrent CoffeScript probably wasn't all on one line.
        */
        /*
            UNION ALL

            SELECT
            COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, MAX(topic_sub.added) AS last_added, CASE WHEN MAX(topic_sub.added) > MAX(comment.added) THEN MAX(topic_sub.added) ELSE MAX(comment.added) END as last_action,
            topic.*,
            topic_creator_user.value AS topic_creator_user_name,
            topic_creator_content.directory AS topic_creator_address,
            topic.topic_id || _ || topic_creator_content.directory AS row_topic_uri,
            topic_sub.topic_id || _ || topic_sub_creator_content.directory AS row_topic_sub_uri,
            (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || _ || topic_creator_content.directory)+1 AS votes
            FROM topic
            LEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)
            LEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = content.json)
            LEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = cert_user_id)
            LEFT JOIN topic AS topic_sub ON (topic_sub.parent_topic_uri = topic.topic_id || _ || topic_creator_content.directory)
            LEFT JOIN json AS topic_sub_creator_json ON (topic_sub_creator_json.json_id = topic_sub.json_id)
            LEFT JOIN json AS topic_sub_creator_content ON (topic_sub_creator_content.directory = topic_sub_creator_json.directory AND topic_sub_creator_content.file_name = content.json)
            LEFT JOIN comment ON (comment.topic_uri = row_topic_sub_uri)
            WHERE topic.type = "group" " + whereb + "
            GROUP BY topic.topic_id
        */
        query += "\nUNION ALL\n\nSELECT\n COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, MAX(topic_sub.added) AS last_added, CASE WHEN MAX(topic_sub.added) > MAX(comment.added) THEN MAX(topic_sub.added) ELSE MAX(comment.added) END as last_action,\n topic.*,\n topic_creator_user.value AS topic_creator_user_name,\n topic_creator_content.directory AS topic_creator_address,\n topic.topic_id || '_' || topic_creator_content.directory AS row_topic_uri,\n topic_sub.topic_id || '_' || topic_sub_creator_content.directory AS row_topic_sub_uri,\n (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || '_' || topic_creator_content.directory)+1 AS votes\nFROM topic\nLEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)\nLEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = 'content.json')\nLEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = 'cert_user_id')\nLEFT JOIN topic AS topic_sub ON (topic_sub.parent_topic_uri = topic.topic_id || '_' || topic_creator_content.directory)\nLEFT JOIN json AS topic_sub_creator_json ON (topic_sub_creator_json.json_id = topic_sub.json_id)\nLEFT JOIN json AS topic_sub_creator_content ON (topic_sub_creator_content.directory = topic_sub_creator_json.directory AND topic_sub_creator_content.file_name = 'content.json')\nLEFT JOIN comment ON (comment.topic_uri = row_topic_sub_uri)\nWHERE topic.type = \"group\" " + whereb + '\nGROUP BY topic.topic_id';
      }
      if (!this.list_all && !this.parent_topic_uri) {
        query += ' ORDER BY last_action DESC LIMIT 30';
      }
      return Page.cmd('dbQuery', [query], (function (_this) {
        return function (topics) {
          // debugger;
          var j, topicsLength, topic, topicParent, topicUri;
          /*
            To Do:
                What's this doing? Adding a breakpoint to the function in topics.sort results in nothing happening.
                What's this doing and why?
          */
          topics.sort(function (a, b) {
            // debugger;
            var boosterA, boosterB;
            boosterA = boosterB = 0;
            if (window.TopicList.topic_sticky_uris[a.row_topic_uri]) {
              boosterA = window.TopicList.topic_sticky_uris[a.row_topic_uri] * 10000000;
            }
            if (window.TopicList.topic_sticky_uris[b.row_topic_uri]) {
              boosterB = window.TopicList.topic_sticky_uris[b.row_topic_uri] * 10000000;
            }
            return Math.max(b.last_comment + boosterB, b.last_added + boosterB) - Math.max(a.last_comment + boosterA, a.last_added + boosterA);
          });
          for (j = 0, topicsLength = topics.length; j < topicsLength; j++) {
            topic = topics[j];
            topicUri = topic.row_topic_uri;
            if (topic.last_added) {
              topic.added = topic.last_added;
            }
            if (_this.parent_topic_uri && topicUri === _this.parent_topic_uri) {
              topicParent = topic;
              continue;
            }
            elem = $('#topic_' + topicUri);
            if (elem.length === 0) {
              elem = $('.topics-list .topic.template').clone().removeClass('template').attr('id', 'topic_' + topicUri);
              if (type !== 'noanim') {
                elem.cssSlideDown();
              }
            }
            elem.insertAfter(lastElem);
            lastElem = elem;
            _this.applyTopicData(elem, topic);
          }
          Page.addInlineEditors();
          $('body').css({
            'overflow': 'auto',
            'height': 'auto'
          });
          if (window.location.href.match(/(\?|&)q=/)) {
            $('#originalZeroTorrent').fadeIn();
            $('html,body').animate({scrollTop: $('.topics-list').offset().top}, 'normal');
          }
          _this.logEnd('Load topics...');
          if (parseInt($('.topics-loading').css('top')) > -30) {
            $('.topics-loading').css('top', '-30px');
          } else {
            $('.topics-loading').remove();
          }
          if (_this.parent_topic_uri) {
            $('.topics-title').html("<span class='parent-link'><a href='?Main'>Main</a> &rsaquo;</span> " + topicParent.title);
          }
          $('.topics').css('opacity', 1);
          if (topics.length === 0) {
            if (Page.site_info.bad_files) {
              $('.message-big').text('Initial sync in progress...');
            } else {
              $('.message-big').text('Welcome to your own forum! :)');
              $('.topic-new-link').trigger('click');
            }
            $('.message-big').css('display', 'block').cssLater('opacity', 1);
          } else {
            $('.message-big').css('display', 'none');
          }
          if (topics.length === 30) {
            $('.topics-more').css('display', 'block');
          } else {
            $('.topics-more').css('display', 'none');
          }
          if (cb) {
            return cb();
          }
        };
      })(this));
    };

    TopicList.prototype.applyTopicData = function (elem, topic, type) {
      var body, imdblink, lastAction, magnet, magnetlink, swarm, titleHash, topicUri, torrent, torrentCategory, torrentImdb, torrentlink, url, urlMatch, visited;
      if (type == null) {
        type = 'list';
      }
      titleHash = Text.toUrl(topic.title);
      topicUri = topic.row_topic_uri;
      $('.title .title-link', elem).text(topic.title);
      $('.title .title-link, a.image, .comment-num', elem).attr('href', '?full&Topic:' + topicUri + '/' + titleHash);
      elem.data('topic_uri', topicUri);
      body = topic.body;
      urlMatch = body.match(/http[s]{0,1}:\/\/[^"', \r\n)$]+/);
      if (topic.type === 'group') {
        $(elem).addClass('topic-group');
        $('.image .icon', elem).removeClass('icon-topic-chat').addClass('icon-topic-group');
        $('.link', elem).css('display', 'none');
        $('.title .title-link, a.image, .comment-num', elem).attr('href', '?Topics:' + topicUri + '/' + titleHash);
      } else if (urlMatch) {
        url = urlMatch[0];
        if (type !== 'show') {
          body = body.replace(/http[s]{0,1}:\/\/[^"' \r\n)$]+$/g, '');
        }
        $('.image .icon', elem).removeClass('icon-topic-chat').addClass('icon-topic-link');
        $('.link', elem).css('display', '').attr('href', Text.fixLink(url));
        $('.link .link-url', elem).text(url);
      } else {
        $('.image .icon', elem).removeClass('icon-topic-link').addClass('icon-topic-chat');
        $('.link', elem).css('display', 'none');
      }
      torrentCategory = topic.category;
      torrentImdb = topic.torrent_imdb;
      swarm = {
        seeders: topic.seeders || 0,
        leechers: topic.leechers || 0
      };
      $('.torrent-seeders').text(swarm.seeders);
      $('.torrent-leechers').text(swarm.leechers);
      if (type === 'show') {
        torrent = topic.torrent_file;
        if (torrent.split('/')[3] === '') {
          torrentlink = '';
          $('.topics-full .topic.topic-full .torrentfile_link').hide();
        } else {
          $('.topics-full .topic.topic-full .torrentfile_link').attr('href', torrent);
          torrentlink = "<a href='" + torrent + "' download>Download torrent</a>";
        }
        magnet = topic.magnet;
        if (magnet === '') {
          magnetlink = '';
          $('.topics-full .topic.topic-full .magnet_link').hide();
        } else {
          $('.topics-full .topic.topic-full .magnet_link').attr('href', magnet);
          magnetlink = "<a href='" + magnet + "'>Magnet Link</a><br />";
        }
        if (torrentImdb === '') {
          imdblink = '';
        } else {
          imdblink = "<a href='http://www.imdb.com/title/" + torrentImdb + "'>IMDB Link</a><br />";
        }
        $('.body', elem).html(Text.toMarked(body, {
          'sanitize': true
        }) + ('<br />Category: ' + torrentCategory + '<br />' + imdblink + ' ' + magnetlink + ' ' + torrentlink));
        $('.topics-full .topic.topic-full .torrent_category').html(torrentCategory);
        $('.topics-full .topic.topic-full .torrent_category').attr('href', '?full&Category:' + torrentCategory);
      } else {
        $('.body', elem).text(body);
      }
      if (window.TopicList.topic_sticky_uris[topicUri]) {
        elem.addClass('topic-sticky');
      }
      if (type !== 'show') {
        lastAction = Math.max(topic.last_comment, topic.added);
        if (topic.type === 'group') {
          $('.comment-num', elem).text('last activity');
          $('.added', elem).text(Time.since(lastAction));
        } else if (topic.comments_num > 0) {
          $('.comment-num', elem).text(topic.comments_num + ' comment');
          $('.added', elem).text('last ' + Time.since(lastAction));
        } else {
          $('.comment-num', elem).text('0 comments');
          $('.added', elem).text(Time.since(lastAction));
        }
      }
      $('.user_name', elem).text(topic.topic_creator_user_name.replace(/@.*/, '')).attr('title', topic.topic_creator_user_name + ': ' + topic.topic_creator_address);
      if (User.my_topic_votes[topicUri]) {
        $('.score-inactive .score-num', elem).text(topic.votes - 1);
        $('.score-active .score-num', elem).text(topic.votes);
        $('.score', elem).addClass('active');
      } else {
        $('.score-inactive .score-num', elem).text(topic.votes);
        $('.score-active .score-num', elem).text(topic.votes + 1);
      }
      $('.score', elem).off('click').on('click', this.submitTopicVote);
      visited = Page.local_storage['topic.' + topicUri + '.visited'];
      if (!visited) {
        elem.addClass('visit-none');
      } else if (visited < lastAction) {
        elem.addClass('visit-newcomment');
      }
      if (type === 'show') {
        $('.added', elem).text(Time.since(topic.added));
      }
      if (topic.topic_creator_address === Page.site_info.auth_address) {
        $(elem).attr('data-object', 'Topic:' + topicUri).attr('data-deletable', 'yes');
        $('.title .title-link', elem).attr('data-editable', 'title').data('content', topic.title);
        return $('.body', elem).attr('data-editable', 'body').data('content', topic.body);
      }
    };

    TopicList.prototype.submitCreateTopic = function () {
      var body, innerPath, title, torrentCategory, torrentFile, torrentFilename, torrentImdb, torrentMagnet;
      if (!Page.site_info.cert_user_id) {
        Page.cmd('wrapperNotification', ['info', 'Please, choose your account before creating a topic.']);
        return false;
      }
      title = $('.topic-new #topic_title').val().trim();
      body = $('.topic-new #topic_body').val().trim();
      torrentMagnet = $('.topic-new #torrent_magnet').val();
      torrentImdb = $('.topic-new #torrent_imdb').val().replace('http://www.imdb.com/title/', '');
      torrentFile = $('.topic-new #filedata').val();
      torrentFilename = $('.topic-new #torrent_file').val().replace('C:\\fakepath\\', '');
      torrentCategory = $('.topic-new #torrent_category').val();
      if (!title) {
        return $('.topic-new #topic_title').focus();
      }
      $('.topic-new .button-submit').addClass('loading');
      innerPath = ('data/users/' + Page.site_info.auth_address + '/') + torrentFilename;
      if (torrentFile) {
        Page.writePublish(innerPath, torrentFile.split(',')[1], (function (_this) {
          return function (res) {
            User.checkCert('updaterules');
            Page.cmd('wrapperNotification', ['info', 'File upload success']);
            return Page.cmd('sitePublish', {
              'inner_path': innerPath
            }, function (res) {
              if (res === 'ok') {
                return Page.cmd('wrapperNotification', ['info', 'Torrent file published.']);
              } else {
                return Page.cmd('wrapperNotification', ['error', res]);
              }
            });
          };
        })(this));
      }
      return User.getData((function (_this) {
        return function (data) {
          var topic;
          topic = {
            'topic_id': data.next_topic_id,
            'category': torrentCategory,
            'title': title,
            'torrent_imdb': torrentImdb,
            'body': body,
            'magnet': torrentMagnet,
            'torrent_file': innerPath,
            'added': Time.timestamp()
          };
          if (_this.parent_topic_uri) {
            topic.parent_topic_uri = _this.parent_topic_uri;
          }
          data.topic.push(topic);
          data.next_topic_id += 1;
          return User.publishData(data, function (res) {
            $('.topic-new .button-submit').removeClass('loading');
            $('.topic-new').slideUp();
            $('.topic-new-link').slideDown();
            setTimeout(function () {
              return _this.loadTopics();
            }, 600);
            $('.topic-new #topic_body').val('');
            $('.topic-new #topic_title').val('');
            return $('.topic-new #torrent_magnet').val('');
          });
        };
      })(this));
    };

    TopicList.prototype.submitTopicVote = function (e) {
      var elem, innerPath;
      console.log(innerPath); // Suppress unused variable warning. Why isn't innerPath used? Does this need to be here?
      if (!Page.site_info.cert_user_id) {
        Page.cmd('wrapperNotification', ['info', 'Please, choose your account before upvoting.']);
        return false;
      }
      elem = $(e.currentTarget);
      elem.toggleClass('active').addClass('loading');
      innerPath = 'data/users/' + User.my_address + '/data.json';
      User.getData((function (_this) {
        return function (data) {
          var topicUri;
          if (data.topic_vote == null) {
            data.topic_vote = {};
          }
          topicUri = elem.parents('.topic').data('topic_uri');
          if (elem.hasClass('active')) {
            data.topic_vote[topicUri] = 1;
          } else {
            delete data.topic_vote[topicUri];
          }
          return User.publishData(data, function (res) {
            return elem.removeClass('loading');
          });
        };
      })(this));
      return false;
    };

    return TopicList;
  })(Class);

  window.TopicList = new TopicList();
}).call(this);
