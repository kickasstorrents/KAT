/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/utils/Menu.coffee ---- */

(function () {
  var Menu;
  var slice = [].slice;

  Menu = (function () {
    function Menu (button) {
      this.button = button;
      this.elem = $('.menu.template').clone().removeClass('template');
      // this.elem.appendTo("body");
      var _this = this;
      _this.elem[0].style.position = 'relative';
      _this.elem[0].style.top = '-2.5em';
      _this.elem[0].style.left = null;
      _this.elem[0].style.right = 0;

      $('.feed-follow-list').get(0).appendChild(_this.elem[0]);
      this.items = [];
    }

    Menu.prototype.show = function () {
      /*eslint-disable no-unused-vars*/var buttonPos; /* eslint-enable*/
      if (window.visible_menu && window.visible_menu.button[0] === this.button[0]) {
        window.visible_menu.hide();
        return this.hide();
      } else {
        buttonPos = this.button.offset();
        this.elem.css({
          'top': '-2.5em', // button_pos.top + this.button.outerHeight(),
          'right': '0' // button_pos.left
        });
        this.button.addClass('menu-active');
        this.elem.addClass('visible');
        if (window.visible_menu) {
          window.visible_menu.hide();
        }
        window.visible_menu = this;
        return window.visible_menu;
      }
    };

    Menu.prototype.hide = function () {
      this.elem.removeClass('visible');
      this.button.removeClass('menu-active');
      window.visible_menu = null;
      return window.visible_menu;
    };

    Menu.prototype.addItem = function (title, cb) {
      var item;
      item = $('.menu-item.template', this.elem).clone().removeClass('template');
      item.html(title);
      item.on('click', (function (_this) {
        return function () {
          if (!cb(item)) {
            _this.hide();
          }
          return false;
        };
      })(this));
      item.appendTo(this.elem);
      this.items.push(item);
      return item;
    };

    Menu.prototype.log = function () {
      var args;
      /*eslint-disable yoda*/args = 1 <= arguments.length ? slice.call(arguments, 0) : [];/* eslint-enable*/
      return console.log.apply(console, ['[Menu]'].concat(slice.call(args)));
    };

    return Menu;
  })();

  window.Menu = Menu;

  $('body').on('click', function (e) {
    if (window.visible_menu && e.target !== window.visible_menu.button[0] && $(e.target).parent()[0] !== window.visible_menu.elem[0]) {
      return window.visible_menu.hide();
    }
  });
}).call(this);
