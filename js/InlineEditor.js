/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/utils/InlineEditor.coffee ---- */

(function () {
  var InlineEditor;
  var bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };

  InlineEditor = (function () {
    function InlineEditor (elem1, getContent, saveContent, getObject) {
      this.elem = elem1;
      this.getContent = getContent;
      this.saveContent = saveContent;
      this.getObject = getObject;
      this.cancelEdit = bind(this.cancelEdit, this);
      this.deleteObject = bind(this.deleteObject, this);
      this.saveEdit = bind(this.saveEdit, this);
      this.stopEdit = bind(this.stopEdit, this);
      this.startEdit = bind(this.startEdit, this);
      this.edit_button = $("<a href='#Edit' class='editable-edit icon-edit'></a>");
      this.edit_button.on('click', this.startEdit);
      this.elem.addClass('editable').before(this.edit_button);
      this.editor = null;
      this.elem.on('mouseenter', (function (_this) {
        return function (e) {
          var scrolltop, top;
          _this.edit_button.css('opacity', '0.4');
          scrolltop = $(window).scrollTop();
          top = _this.edit_button.offset().top - parseInt(_this.edit_button.css('margin-top'));
          if (scrolltop > top) {
            return _this.edit_button.css('margin-top', scrolltop - top + e.clientY - 20);
          } else {
            return _this.edit_button.css('margin-top', '');
          }
        };
      })(this));
      this.elem.on('mouseleave', (function (_this) {
        return function () {
          return _this.edit_button.css('opacity', '');
        };
      })(this));
      if (this.elem.is(':hover')) {
        this.elem.trigger('mouseenter');
      }
    }

    InlineEditor.prototype.startEdit = function () {
      var j, results;
      this.content_before = this.elem.html();
      this.editor = $("<textarea class='editor'></textarea>");
      this.editor.val(this.getContent(this.elem, 'raw'));
      this.elem.after(this.editor);
      $('.editbg').css('display', 'block').cssLater('opacity', 0.9, 10);
      this.elem.html(function () {
        results = [];
        for (j = 1; j <= 50; j++) { results.push(j); }
        return results;
      }.apply(this).join('fill the width'));
      this.copyStyle(this.elem, this.editor);
      this.elem.html(this.content_before);
      this.autoExpand(this.editor);
      this.elem.css('display', 'none');
      if ($(window).scrollTop() === 0) {
        this.editor[0].selectionEnd = 0;
        this.editor.focus();
      }
      $('.editable-edit').css('display', 'none');
      $('.editbar').css('display', 'inline-block').addClassLater('visible', 10);
      $('.publishbar').css('opacity', 0);
      $('.editbar .object').text(this.getObject(this.elem).data('object') + '.' + this.elem.data('editable'));
      $('.editbar .button').removeClass('loading');
      $('.editbar .save').off('click').on('click', this.saveEdit);
      $('.editbar .delete').off('click').on('click', this.deleteObject);
      $('.editbar .cancel').off('click').on('click', this.cancelEdit);
      if (this.getObject(this.elem).data('deletable')) {
        $('.editbar .delete').css('display', '').html('Delete ' + this.getObject(this.elem).data('object').split(':')[0]);
      } else {
        $('.editbar .delete').css('display', 'none');
      }
      window.onbeforeunload = function () {
        return 'Your unsaved blog changes will be lost!';
      };
      return false;
    };

    InlineEditor.prototype.stopEdit = function () {
      if (this.editor) {
        this.editor.remove();
      }
      this.editor = null;
      this.elem.css('display', '');
      $('.editbg').css('opacity', 0).cssLater('display', 'none', 300);
      $('.editable-edit').css('display', '');
      $('.editbar').cssLater('display', 'none', 1000).removeClass('visible');
      $('.publishbar').css('opacity', 1);
      window.onbeforeunload = null;
      return window.onbeforeunload;
    };

    InlineEditor.prototype.saveEdit = function () {
      var content;
      content = this.editor.val();
      $('.editbar .save').addClass('loading');
      this.saveContent(this.elem, content, (function (_this) {
        return function (contentHtml) {
          if (contentHtml) {
            $('.editbar .save').removeClass('loading');
            _this.stopEdit();
            if (typeof contentHtml === 'string') {
              _this.elem.html(contentHtml);
            }
            return $('pre code').each(function (i, block) {
              return hljs.highlightBlock(block);
            });
          } else {
            return $('.editbar .save').removeClass('loading');
          }
        };
      })(this));
      return false;
    };

    InlineEditor.prototype.deleteObject = function () {
      var objectType;
      objectType = this.getObject(this.elem).data('object').split(':')[0];
      Page.cmd('wrapperConfirm', ['Are you sure you sure to delete this ' + objectType + '?', 'Delete'], (function (_this) {
        return function (confirmed) {
          $('.editbar .delete').addClass('loading');
          return Page.saveContent(_this.getObject(_this.elem), null, function () {
            return _this.stopEdit();
          });
        };
      })(this));
      return false;
    };

    InlineEditor.prototype.cancelEdit = function () {
      this.stopEdit();
      this.elem.html(this.content_before);
      $('pre code').each(function (i, block) {
        return hljs.highlightBlock(block);
      });
      return false;
    };

    InlineEditor.prototype.copyStyle = function (elemFrom, elemTo) {
      var fromStyle;
      elemTo.addClass(elemFrom[0].className);
      /*eslint-disable no-undef*/romStyle = getComputedStyle(elemFrom[0]);/* eslint-enable*/
      elemTo.css({
        fontFamily: fromStyle.fontFamily,
        fontSize: fromStyle.fontSize,
        fontWeight: fromStyle.fontWeight,
        marginTop: fromStyle.marginTop,
        marginRight: fromStyle.marginRight,
        marginBottom: fromStyle.marginBottom,
        marginLeft: fromStyle.marginLeft,
        paddingTop: fromStyle.paddingTop,
        paddingRight: fromStyle.paddingRight,
        paddingBottom: fromStyle.paddingBottom,
        paddingLeft: fromStyle.paddingLeft,
        lineHeight: fromStyle.lineHeight,
        textAlign: fromStyle.textAlign,
        color: fromStyle.color,
        letterSpacing: fromStyle.letterSpacing
      });
      if (elemFrom.innerWidth() < 1000) {
        return elemTo.css('minWidth', elemFrom.innerWidth());
      }
    };

    InlineEditor.prototype.autoExpand = function (elem) {
      var editor;
      editor = elem[0];
      elem.height(1);
      elem.on('input', function () {
        if (editor.scrollHeight > elem.height()) {
          return elem.height(1).height(editor.scrollHeight + parseFloat(elem.css('borderTopWidth')) + parseFloat(elem.css('borderBottomWidth')));
        }
      });
      elem.trigger('input');
      return elem.on('keydown', function (e) {
        var s, val;
        if (e.which === 9) {
          e.preventDefault();
          s = this.selectionStart;
          val = elem.val();
          elem.val(val.substring(0, this.selectionStart) + '\t' + val.substring(this.selectionEnd));
          this.selectionEnd = s + 1;
          return this.selectionEnd;
        }
      });
    };

    return InlineEditor;
  })();

  window.InlineEditor = InlineEditor;
}).call(this);
