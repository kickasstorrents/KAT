/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/lib/jquery.cssanim.coffee ---- */


(function() {
  jQuery.fn.cssSlideDown = function() {
    var elem;
    elem = this;
    elem.css({
      "opacity": 0,
      "margin-bottom": 0,
      "margin-top": 0,
      "padding-bottom": 0,
      "padding-top": 0,
      "display": "none",
      "transform": "scale(0.8)"
    });
    setTimeout((function() {
      var height;
      elem.css("display", "");
      height = elem.outerHeight();
      elem.css({
        "height": 0,
        "display": ""
      }).cssLater("transition", "all 0.3s ease-out", 20);
      elem.cssLater({
        "height": height,
        "opacity": 1,
        "margin-bottom": "",
        "margin-top": "",
        "padding-bottom": "",
        "padding-top": "",
        "transform": "scale(1)"
      }, null, 40);
      return elem.one(transitionEnd, function() {
        return elem.css("transition", "").css("transform", "");
      });
    }), 300);
    return this;
  };

  jQuery.fn.fancySlideDown = function() {
    var elem;
    elem = this;
    return elem.css({
      "opacity": 0,
      "transform": "scale(0.9)"
    }).slideDown().animate({
      "opacity": 1,
      "scale": 1
    }, {
      "duration": 600,
      "queue": false,
      "easing": "easeOutBack"
    });
  };

  jQuery.fn.fancySlideUp = function() {
    var elem;
    elem = this;
    return elem.css("backface-visibility", "hidden").delay(600).slideUp(600).animate({
      "opacity": 0,
      "scale": 0.9
    }, {
      "duration": 600,
      "queue": false,
      "easing": "easeOutQuad"
    });
  };

  window.transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend';

}).call(this);


/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/lib/jquery.cssanim.js ---- */


jQuery.cssHooks['scale'] = {
	get: function(elem, computed, extra) {
		var match = window.getComputedStyle(elem).transform.match("[0-9\.]+")
		if (match) {
			var scale = parseFloat(match[0])
			return scale
		} else {
			return 1.0
		}
	},
	set: function(elem, val) {
		//var transforms = $(elem).css("transform").match(/[0-9\.]+/g)
		var transforms = window.getComputedStyle(elem).transform.match(/[0-9\.]+/g)
		if (transforms) {
			transforms[0] = val
			transforms[3] = val
			//$(elem).css("transform", 'matrix('+transforms.join(", ")+")")
			elem.style.transform = 'matrix('+transforms.join(", ")+')'
		} else {
			elem.style.transform = "scale("+val+")"
		}
	}
}

jQuery.fx.step.scale = function(fx) {
	jQuery.cssHooks['scale'].set(fx.elem, fx.now)
};
