/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/APITopicList.coffee ---- */

(function () {
  var APITopicList;
  var bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };
  var extend = function (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) child[key] = parent[key];
    }
    function Ctor () {
      this.constructor = child;
    }
    Ctor.prototype = parent.prototype;
    child.prototype = new Ctor();
    child.__super__ = parent.prototype;
    return child;
  };
  var hasProp = {}.hasOwnProperty;

  APITopicList = (function (superClass) {
    extend(APITopicList, superClass);

    function APITopicList () {
      this.submitTopicVote = bind(this.submitTopicVote, this);
      this.thread_sorter = null;
      this.parent_topic_uri = void 0;
      this.topic_parent_uris = {};
      this.topic_sticky_uris = {};
    }

    APITopicList.prototype.actionList = function (parentTopicId, parentTopicUserAddress, category, search) {
      if (category == null) {
        category = false;
      }
      if (search == null) {
        search = false;
      }
      $('body').empty;
      return this.loadTopics('noanim', false, search, category);
    };

    APITopicList.prototype.loadTopics = function (type, cb, search, category) {
      /*eslint-disable no-unused-vars*/var lastElem, query, where, whereb;/* eslint-enable*/
      if (type == null) {
        type = 'list';
      }
      if (cb == null) {
        cb = false;
      }
      if (search == null) {
        search = false;
      }
      if (category == null) {
        category = false;
      }
      this.logStart('Load topics...');
      if (this.parent_topic_uri) {
        where = "WHERE parent_topic_uri = '" + this.parent_topic_uri + "' OR row_topic_uri = '" + this.parent_topic_uri + "'";
      } else {
        where = 'WHERE topic.type IS NULL AND topic.parent_topic_uri IS NULL ';
      }
      if (category === false || category === 'all') {

      } else {
        whereb = ' AND topic.category LIKE "' + category + '"';
        if (where) {
          where = where + ' AND topic.category LIKE "' + category + '"';
        } else {
          where = 'WHERE topic.category LIKE "' + category + '"';
        }
      }
      if (search !== false) {
        whereb = whereb + ' AND topic.title LIKE "%' + search + '%"';
        if (where) {
          where = where + ' AND topic.title LIKE "%' + search + '%"';
        } else {
          where = 'WHERE topic.title LIKE "%' + search + '%"';
        }
      } else {
        whereb = '';
      }
      lastElem = $('.topics-list .topic.template');
      query = "SELECT\n COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, topic.added as last_added,\n topic.*,\n topic_creator_user.value AS topic_creator_user_name,\n topic_creator_content.directory AS topic_creator_address,\n topic.topic_id || '_' || topic_creator_content.directory AS row_topic_uri,\n NULL AS row_topic_sub_uri,\n (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || '_' || topic_creator_content.directory)+1 AS votes\nFROM topic\nLEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)\nLEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = 'content.json')\nLEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = 'cert_user_id')\nLEFT JOIN comment ON (comment.topic_uri = row_topic_uri)\n" + where + '\nGROUP BY topic.topic_id, topic.json_id';
      console.log(query);
      if (!this.parent_topic_uri) {
        query += "\nUNION ALL\n\nSELECT\n COUNT(comment_id) AS comments_num, MAX(comment.added) AS last_comment, MAX(topic_sub.added) AS last_added,\n topic.*,\n topic_creator_user.value AS topic_creator_user_name,\n topic_creator_content.directory AS topic_creator_address,\n topic.topic_id || '_' || topic_creator_content.directory AS row_topic_uri,\n topic_sub.topic_id || '_' || topic_sub_creator_content.directory AS row_topic_sub_uri,\n (SELECT COUNT(*) FROM topic_vote WHERE topic_vote.topic_uri = topic.topic_id || '_' || topic_creator_content.directory)+1 AS votes\nFROM topic\nLEFT JOIN json AS topic_creator_json ON (topic_creator_json.json_id = topic.json_id)\nLEFT JOIN json AS topic_creator_content ON (topic_creator_content.directory = topic_creator_json.directory AND topic_creator_content.file_name = 'content.json')\nLEFT JOIN keyvalue AS topic_creator_user ON (topic_creator_user.json_id = topic_creator_content.json_id AND topic_creator_user.key = 'cert_user_id')\nLEFT JOIN topic AS topic_sub ON (topic_sub.parent_topic_uri = topic.topic_id || '_' || topic_creator_content.directory)\nLEFT JOIN json AS topic_sub_creator_json ON (topic_sub_creator_json.json_id = topic_sub.json_id)\nLEFT JOIN json AS topic_sub_creator_content ON (topic_sub_creator_content.directory = topic_sub_creator_json.directory AND topic_sub_creator_content.file_name = 'content.json')\nLEFT JOIN comment ON (comment.topic_uri = row_topic_sub_uri)\nWHERE topic.type = \"group\" \nGROUP BY topic.topic_id";
      }
      return Page.cmd('dbQuery', [query], (function (_this) {
        return function (topics) {
          topics.sort(function (a, b) {
            var boosterA, boosterB;
            boosterA = boosterB = 0;
            if (window.APITopicList.topic_sticky_uris[a.row_topic_uri]) {
              boosterA = window.APITopicList.topic_sticky_uris[a.row_topic_uri] * 10000000;
            }
            if (window.APITopicList.topic_sticky_uris[b.row_topic_uri]) {
              boosterB = window.APITopicList.topic_sticky_uris[b.row_topic_uri] * 10000000;
            }
            return Math.max(b.last_comment + boosterB, b.last_added + boosterB) - Math.max(a.last_comment + boosterA, a.last_added + boosterA);
          });
          console.log('set body Text');
          $('body').html(JSON.stringify(topics));
          console.log(JSON.stringify(topics));
          _this.logEnd('Load topics...');
          if (parseInt($('.topics-loading').css('top')) > -30) {
            $('.topics-loading').css('top', '-30px');
          } else {
            $('.topics-loading').remove();
          }
          if (_this.parent_topic_uri) {
            /*eslint-disable no-undef*/$('.topics-title').html("<span class='parent-link'><a href='?Main'>Main</a> &rsaquo;</span> " + topic_parent.title);/* eslint-enable*/
          }
          $('.topics').css('opacity', 1);
          if (cb) {
            return cb();
          }
        };
      })(this));
    };

    APITopicList.prototype.applyTopicData = function (elem, topic, type) {
      /*eslint-disable no-unused-vars*/var body, lastAction, magnet, titleHash, topicUri, torrent, torrentUrl, torrentCategory, torrentFile, torrentString, url, urlMatch, visited;/* eslint-enable*/
      if (type == null) {
        type = 'list';
      }
      titleHash = Text.toUrl(topic.title);
      topicUri = topic.row_topic_uri;
      $('.title .title-link', elem).text(topic.title);
      $('.title .title-link, a.image, .comment-num', elem).attr('href', '?full&Topic:' + topicUri + '/' + titleHash);
      elem.data('topic_uri', topicUri);
      body = topic.body;
      urlMatch = body.match(/http[s]{0,1}:\/\/[^"', \r\n)$]+/);
      if (topic.type === 'group') {
        $(elem).addClass('topic-group');
        $('.image .icon', elem).removeClass('icon-topic-chat').addClass('icon-topic-group');
        $('.link', elem).css('display', 'none');
        $('.title .title-link, a.image, .comment-num', elem).attr('href', '?Topics:' + topicUri + '/' + titleHash);
      } else if (urlMatch) {
        url = urlMatch[0];
        if (type !== 'show') {
          body = body.replace(/http[s]{0,1}:\/\/[^"' \r\n)$]+$/g, '');
        }
        $('.image .icon', elem).removeClass('icon-topic-chat').addClass('icon-topic-link');
        $('.link', elem).css('display', '').attr('href', Text.fixLink(url));
        $('.link .link-url', elem).text(url);
      } else {
        $('.image .icon', elem).removeClass('icon-topic-link').addClass('icon-topic-chat');
        $('.link', elem).css('display', 'none');
      }
      torrentCategory = topic.category;
      if (type === 'show') {
        torrent = topic.torrent_file;
        torrentString = torrent.split(',')[1];
        /*eslint-disable no-undef*/torrentFile = new Blob([torrentString], {
          type: 'application/x-bittorrent'
        });/* eslint-enable*/
        magnet = topic.magnet;
        torrentUrl = URL.createObjectURL(torrentFile);
        $('.body', elem).html(Text.toMarked(body, {
          'sanitize': true
        }) + '<br />Category: ' + torrentCategory + '<br />Magnet: ' + magnet + '<br /><a href="' + torrent + '" download>Download torrent</a>');
      } else {
        $('.body', elem).text(body);
      }
      if (window.APITopicList.topic_sticky_uris[topicUri]) {
        elem.addClass('topic-sticky');
      }
      if (type !== 'show') {
        lastAction = Math.max(topic.last_comment, topic.added);
        if (topic.type === 'group') {
          $('.comment-num', elem).text('last activity');
          $('.added', elem).text(Time.since(lastAction));
        } else if (topic.comments_num > 0) {
          $('.comment-num', elem).text(topic.comments_num + ' comment');
          $('.added', elem).text('last ' + Time.since(lastAction));
        } else {
          $('.comment-num', elem).text('0 comments');
          $('.added', elem).text(Time.since(lastAction));
        }
      }
      $('.user_name', elem).text(topic.topic_creator_user_name.replace(/@.*/, '')).attr('title', topic.topic_creator_user_name + ': ' + topic.topic_creator_address);
      if (User.my_topic_votes[topicUri]) {
        $('.score-inactive .score-num', elem).text(topic.votes - 1);
        $('.score-active .score-num', elem).text(topic.votes);
        $('.score', elem).addClass('active');
      } else {
        $('.score-inactive .score-num', elem).text(topic.votes);
        $('.score-active .score-num', elem).text(topic.votes + 1);
      }
      $('.score', elem).off('click').on('click', this.submitTopicVote);
      visited = Page.local_storage['topic.' + topicUri + '.visited'];
      if (!visited) {
        elem.addClass('visit-none');
      } else if (visited < lastAction) {
        elem.addClass('visit-newcomment');
      }
      if (type === 'show') {
        $('.added', elem).text(Time.since(topic.added));
      }
      if (topic.topic_creator_address === Page.site_info.auth_address) {
        $(elem).attr('data-object', 'Topic:' + topicUri).attr('data-deletable', 'yes');
        $('.title .title-link', elem).attr('data-editable', 'title').data('content', topic.title);
        return $('.body', elem).attr('data-editable', 'body').data('content', topic.body);
      }
    };

    APITopicList.prototype.submitCreateTopic = function () {
      var body, title, torrentCategory, torrentFile, torrentFilename, torrentMagnet;
      if (!Page.site_info.cert_user_id) {
        Page.cmd('wrapperNotification', ['info', 'Please, your choose account before creating a topic.']);
        return false;
      }
      title = $('.topic-new #topic_title').val();
      body = $('.topic-new #topic_body').val();
      torrentMagnet = $('.topic-new #torrent_magnet').val();
      torrentFile = $('.topic-new #filedata').val();
      torrentFilename = $('.topic-new #torrent_file').val().replace('C:\\fakepath\\', '');
      torrentCategory = $('.topic-new #torrent_category').val();
      if (!title) {
        return $('.topic-new #topic_title').focus();
      }
      $('.topic-new .button-submit').addClass('loading');
      return User.getData((function (_this) {
        return function (data) {
          var innerPath, topic;
          innerPath = ('data/users/' + Page.site_info.auth_address + '/') + torrentFilename;
          topic = {
            'topic_id': data.next_topic_id,
            'category': torrentCategory,
            'title': title,
            'body': body,
            'magnet': torrentMagnet,
            'torrent_file': innerPath,
            'added': Time.timestamp()
          };
          console.log(innerPath);
          console.log(torrentFile.split(',')[1]);
          Page.writePublish(innerPath, torrentFile.split(',')[1], function (res) {
            User.checkCert('updaterules');
            return Page.cmd('wrapperNotification', ['info', 'File upload success']);
          });
          if (_this.parent_topic_uri) {
            topic.parent_topic_uri = _this.parent_topic_uri;
          }
          data.topic.push(topic);
          data.next_topic_id += 1;
          console.log('get this far');
          return User.publishData(data, function (res) {
            $('.topic-new .button-submit').removeClass('loading');
            $('.topic-new').slideUp();
            $('.topic-new-link').slideDown();
            setTimeout(function () {
              return _this.loadTopics();
            }, 600);
            $('.topic-new #topic_body').val('');
            $('.topic-new #topic_title').val('');
            return $('.topic-new #torrent_magnet').val('');
          });
        };
      })(this));
    };

    APITopicList.prototype.submitTopicVote = function (e) {
      /*eslint-disable no-unused-vars*/var elem, innerPath;/* eslint-enable*/
      if (!Page.site_info.cert_user_id) {
        Page.cmd('wrapperNotification', ['info', 'Please, your choose account before upvoting.']);
        return false;
      }
      elem = $(e.currentTarget);
      elem.toggleClass('active').addClass('loading');
      innerPath = 'data/users/' + User.my_address + '/data.json';
      User.getData((function (_this) {
        return function (data) {
          var topicUri;
          if (data.topic_vote == null) {
            data.topic_vote = {};
          }
          topicUri = elem.parents('.topic').data('topic_uri');
          if (elem.hasClass('active')) {
            data.topic_vote[topicUri] = 1;
          } else {
            delete data.topic_vote[topicUri];
          }
          return User.publishData(data, function (res) {
            return elem.removeClass('loading');
          });
        };
      })(this));
      return false;
    };

    return APITopicList;
  })(Class);

  window.APITopicList = new APITopicList();
}).call(this);
