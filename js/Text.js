/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/utils/Text.coffee ---- */

(function () {
  var Renderer, Text;
  var extend = function (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) child[key] = parent[key];
    }
    function Ctor () {
      this.constructor = child;
    }
    Ctor.prototype = parent.prototype;
    child.prototype = new Ctor();
    child.__super__ = parent.prototype;
    return child;
  };
  var hasProp = {}.hasOwnProperty;

  Renderer = (function (superClass) {
    extend(Renderer, superClass);

    function Renderer () {
      return Renderer.__super__.constructor.apply(this, arguments);
    }

    Renderer.prototype.image = function (href, title, text) {
      return '<code>![' + text + '](' + href + ')</code>';
    };

    return Renderer;
  })(marked.Renderer);

  Text = (function () {
    function Text () {}

    Text.prototype.toColor = function (text) {
      var hash, i, j, ref;
      hash = 0;
      /*
        To Do:
          * Figure out what this is doing and what it's for
          * Re-write it in a more succint way so eslint doesn't complain
      */
      /* eslint-disable yoda*/
      for (i = j = 0, ref = text.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
        hash += text.charCodeAt(i) * i;
      }
      return 'hsl(' + (hash % 360) + ',30%,50%)';
      /* eslint-enable*/
      /*
      		for i in [0..2]
      			value = (hash >> (i * 8)) & 0xFF
      			color += ('00' + value.toString(16)).substr(-2)
      		return color
       */
    };

    Text.prototype.toMarked = function (text, options) {
      if (options == null) {
        options = {};
      }
      options['gfm'] = true;
      options['breaks'] = true;
      /*eslint-disable no-undef*/ options['renderer'] = renderer; /* eslint-enable*/
      text = this.fixReply(text);
      text = marked(text, options);
      return this.fixHtmlLinks(text);
    };

    Text.prototype.fixHtmlLinks = function (text) {
      if (window.is_proxy) {
        return text.replace(/href="http:\/\/(127.0.0.1|localhost):43110/g, 'href="http://zero');
      } else {
        return text.replace(/href="http:\/\/(127.0.0.1|localhost):43110/g, 'href="');
      }
    };

    Text.prototype.fixLink = function (link) {
      var back;
      if (window.is_proxy) {
        back = link.replace(/http:\/\/(127.0.0.1|localhost):43110/, 'http://zero');
        return back.replace(/http:\/\/zero\/([^\/]+\.bit)/, 'http://$1');
      } else {
        return link.replace(/http:\/\/(127.0.0.1|localhost):43110/, '');
      }
    };

    Text.prototype.toUrl = function (text) {
      return text.replace(/[^A-Za-z0-9]/g, '+').replace(/[+]+/g, '+').replace(/[+]+$/, '');
    };

    Text.prototype.fixReply = function (text) {
      return text.replace(/(>.*\n)([^\n>])/gm, '$1\n$2');
    };

    Text.prototype.toBitcoinAddress = function (text) {
      return text.replace(/[^A-Za-z0-9]/g, '');
    };

    Text.prototype.jsonEncode = function (obj) {
      return /*eslint-disable no-undef*/btoa(unescape(encodeURIComponent(JSON.stringify(obj, void 0, '\t'))));/* eslint-enable*/
    };

    return Text;
  })();

  window.is_proxy = window.location.pathname === '/';

  window.renderer = new Renderer();

  window.Text = new Text();
}).call(this);
