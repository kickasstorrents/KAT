/* ---- data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/js/ZeroTalk.coffee ---- */

(function () {
  var ZeroTalk, NotImplemented;
  var bind = function (fn, me) {
    return function () {
      return fn.apply(me, arguments);
    };
  };
  var extend = function (child, parent) {
    for (var key in parent) {
      if (hasProp.call(parent, key)) child[key] = parent[key];
    }
    function Ctor () {
      this.constructor = child;
    }
    Ctor.prototype = parent.prototype;
    child.prototype = new Ctor();
    child.__super__ = parent.prototype;
    return child;
  };
  var hasProp = {}.hasOwnProperty;

  NotImplemented = window.NotImplemented = function (errType) {
    if (errType) {
      Page.cmd('wrapperNotification', ['error', errType + ' are currently not implemented!', 6000]);
    } else {
      Page.cmd('wrapperNotification', ['error', 'This functionality has not currently been implemented!', 6000]);
    }
  };

  ZeroTalk = (function (superClass) {
    extend(ZeroTalk, superClass);

    function ZeroTalk () {
      this.setSiteinfo = bind(this.setSiteinfo, this);
      this.actionSetSiteInfo = bind(this.actionSetSiteInfo, this);
      this.saveContent = bind(this.saveContent, this);
      this.getObject = bind(this.getObject, this);
      this.getContent = bind(this.getContent, this);
      this.onOpenWebsocket = bind(this.onOpenWebsocket, this);
      return ZeroTalk.__super__.constructor.apply(this, arguments);
    }

    ZeroTalk.prototype.init = function () {
      var i, len, ref, textarea;
      this.log('inited!');
      this.site_info = null;
      this.server_info = null;
      this.local_storage = {};
      this.site_address = null;
      ref = $('textarea');
      for (i = 0, len = ref.length; i < len; i++) {
        textarea = ref[i];
        this.autoExpand($(textarea));
      }
      return $('.editbar .icon-help').on('click', (function (_this) {
        return function () {
          $('.editbar .markdown-help').css('display', 'block');
          $('.editbar .markdown-help').toggleClassLater('visible', 10);
          $('.editbar .icon-help').toggleClass('active');
          return false;
        };
      })(this));
    };

    ZeroTalk.prototype.onOpenWebsocket = function (e) {
      this.cmd('wrapperSetViewport', 'width=device-width, initial-scale=1.0');
      this.cmd('wrapperGetLocalStorage', [], (function (_this) {
        return function (res) {
          if (res == null) {
            res = {};
          }
          _this.local_storage = res;
          return _this.local_storage;
        };
      })(this));
      this.cmd('siteInfo', {}, (function (_this) {
        return function (site) {
          _this.site_address = site.address;
          _this.setSiteinfo(site);
          return User.updateMyInfo(function () {
            return _this.routeUrl(window.location.search.substring(1));
          });
        };
      })(this));
      return this.cmd('serverInfo', {}, (function (_this) {
        return function (ret) {
          var version;
          _this.server_info = ret;
          version = parseInt(_this.server_info.version.replace(/\./g, ''));
          if (version < 31) {
            return _this.cmd('wrapperNotification', ['error', 'ZeroTalk requires ZeroNet 0.3.1, please update!']);
          }
        };
      })(this));
    };

    ZeroTalk.prototype.onPageLoaded = function () {
      return $('body').addClass('loaded');
    };

    ZeroTalk.prototype.autocompleteSearch = function () {
      $('#contentSearch').autocomplete({
        cache: !0,
        minLength: 2,
        open: function (e, t) {
          $('#contentSearch').autocomplete('widget').addClass('ui-search-autocomplete');
        },
        source: function (e, t) {
          if (e.term.length === 0) return;
          Page.cmd('dbQuery', [
            "SELECT (title) FROM topic WHERE title LIKE '" +
                e.term.replace(/'/g, "''") +
                "%' ORDER BY added DESC LIMIT 100"
          ], function (res) {
            if (res.length !== 0) {
              t(res.map(function (i) {
                return {
                  label: i.title,
                  value: e.title
                };
              }));
            } else {
              Page.cmd('dbQuery', [
                "SELECT (title) FROM topic WHERE title LIKE '%" +
                        e.term.replace(/'/g, "''") +
                        "%' ORDER BY added DESC LIMIT 100"
              ], function (res) {
                t(res.map(function (i) {
                  return {
                    label: i.title,
                    value: e.title
                  };
                }));
              });
            }
          });
        },
        select: function (e, t) {
          if (!window.location.href.match(/(\??|&?)q=/)) {
            window.location = '?full&q=' + t.item.value;
          } else {
            TopicList.loadTopics('list', false, t.item.value, $('#torrent_search_category').val());
          }
        }
      });
    };

    ZeroTalk.prototype.routeUrl = function (url) {
      var _this = this;
      var match = false;
      this.log('Routing url:', url);
      /*
        To Do:
          * Remove me...
          * This deliberate hack can stay for now...
      */
      if (match) {
        $('body').addClass('page-topics');
        return TopicList.actionList(parseInt(match[1]), Text.toBitcoinAddress(match[2]));
      } else if ((match = (function () {
        /*
          To Do:
            Display categories properly which will allow me to remove this temporary hack that stops a
            blank page being displayed when navigating categories.
        */
        if (url.match(/Category:([0-9a-zA-Z]+)/)) {
          return false;
        } else {
          return url.match(/Category:([0-9a-zA-Z]+)/);
        }
      }()))) {
        $('body').addClass('page-topics');
        $('#' + match[1]).attr('selected', 'selected');
        return TopicList.actionList(null, null, match[1]);
      } else if ((match = url.match(/API_GET_([0-9a-zA-Z]+)_([0-9a-zA-Z]+)/))) {
        return APITopicList.actionList(match[1], match[2]);
      } else {
        if (window.location.search.match(/(\??|&?)full(&?)/)) {
          var template;
          if (window.location.search.match(/(\??|&?)full&Topic:.*(&?)/)) {
            template = 'views/topic.html';
          } else {
            template = 'views/full.html';
          }
          /*
            To Do:
              * Split views/full.html into two files as follows:
                * layout.html - Header and sidebar
                * full.html - Fron page torrents
          */
          Page.cmd('fileGet', [template], function (data) {
            document.body.className = 'mainBody';
            document.getElementById('wrapper').innerHTML = data;
            if (Page.site_info.cert_user_id) {
              $('.upload-link').show();
            }
            if (template === 'views/topic.html') {
              _this.autocompleteSearch();
              (function (e, t, i) {
                'use strict';
                e.fn.tabs = function (n) {
                  var s = null;
                  n = e.extend({
                    useHash: !0,
                    default: null,
                    tabSelector: '> div',
                    linkSelector: '.tabNavigation:first a',
                    selectedTabClass: 'selectedTab',
                    onShow: function () {}
                  }, n);

                  function r () {
                    var e = t.hash.substring(1);
                    e = e.replace(/_\d+$/, '');
                    e = e.replace(/_.+$/, '');
                    return e;
                  }
                  e(this).each(function () {
                    var l = e(n.tabSelector, this);
                    var a = e(n.linkSelector, this);
                    l.hide();

                    function u (e, r) {
                      if (!e) { return; }
                      a.removeClass(n.selectedTabClass);
                      l.hide();
                      l.filter('#' + e).show();
                      l.filter('#tab-' + e).show();
                      a.filter('[rel=' + e + ']').addClass(n.selectedTabClass);
                      if (n.useHash && r) {
                        if (i.replaceState) {
                          i.replaceState({}, '', '#' + e);
                        } else {
                          t.hash = e;
                        }
                      }
                      s = e;
                    }
                    var o = e();
                    var c = r();
                    if (n.useHash && c) {
                      o = a.filter('[rel=' + c + ']');
                    }
                    if (!o.length && n.default) {
                      o = a.filter('[rel=' + n.default + ']');
                    }
                    if (!o.length) {
                      o = a.filter('.' + n.selectedTabClass);
                    }
                    if (!o.length) {
                      o = a.filter(':first');
                    }
                    u(o.prop('rel'));
                    a.click(function () {
                      n.onShow(s, e(this));
                      u(e(this).prop('rel'), !0);
                      return !1;
                    });
                  });
                  return this;
                };
                e(function () {
                  e('.tabSwitcher').tabs();
                });
              /*eslint-disable no-undef*/ })(jQuery, document.location, history);/* eslint-enable*/
              $('searchform').attr('onsubmit', '');
              document.forms.searchform.onsubmit = function () {
                if (this.contentSearch.value.length > 1) {
                  window.location.href = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1] + '/?full&q=' + encodeURIComponent((this.contentSearch.value));
                }
                return false;
              };
            } else {
              _this.autocompleteSearch();
              // Load any other categories from the SQLite database and add them to the select field above...
              Page.cmd('fileGet', ['categories.json'], function (categories) {
                categories = JSON.parse(categories);
                var opt, cat, appendCategory;
                for (var category in categories) {
                  appendCategory = true;
                  cat = ((category === 'xxx') ? 'XXX' : ((category === 'tv') ? 'TV' : category.substring(0, 1).toUpperCase() + category.substring(1, category.length)));
                  for (var j = 0; j < document.getElementById('torrent_category').getElementsByTagName('option').length; j++) {
                    if (document.getElementById('torrent_category').getElementsByTagName('option')[j].innerHTML === cat) {
                      appendCategory = false;
                      break;
                    }
                  }
                  if (appendCategory) {
                    opt = document.createElement('option');
                    opt.value = cat.toLowerCase();
                    opt.innerHTML = cat;
                    document.getElementById('torrent_category').appendChild(opt);

                    opt = document.createElement('option');
                    opt.value = cat.toLowerCase();
                    opt.innerHTML = cat;
                    document.getElementById('torrent_search_category').appendChild(opt);
                  }
                }
              });
            }
            match = url.match(/Topic:([0-9]+)_([0-9a-zA-Z]+)/);
            if (match) {
              $('body').addClass('page-topic');
              return TopicShow.actionShow(parseInt(match[1]), Text.toBitcoinAddress(match[2]));
            } else {
              $('body').addClass('page-main');
              return TopicList.actionList();
            }
          });
        } else {
          document.body.className = 'landing-page';
          Page.cmd('fileGet', ['views/landing.html'], function (data) {
            $('#originalZeroTorrent').css('display', 'none');
            document.getElementById('wrapper').innerHTML = data;
            _this.autocompleteSearch();
            document.forms.landingSearchform.action = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1];
            document.forms.landingSearchform.onsubmit = function () {
              if (document.forms.landingSearchform.q.value.length > 0) {
                        // Build the search URL
                window.location.href = window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1] + '/?full&q=' + encodeURIComponent(document.forms.landingSearchform.q.value);
              }
              return false; // Return false because the search is empty
            };
            document.forms.landingSearchform.contentSearch.onkeypress = function (e) {
              if (e.keyCode === 13) {
                document.forms.landingSearchform.landingSearchFormSubmit.click(); // Simulate a click on the submit button (i.e submit the form)
              }
            };
          });
        }
        setTimeout(function () {
          $('#latestForum a').click(function (e) { e.preventDefault(); return NotImplemented('Forums'); });
          $('#navigation .community-link').click(function (e) { e.preventDefault(); return NotImplemented('Forums'); });
          $('#blogroll a').click(function (e) { e.preventDefault(); return NotImplemented('Blog Posts'); });
          $('#navigation .blog-link').click(function (e) { e.preventDefault(); return NotImplemented('Blog Posts'); });
          $('.ka-rss').click(function (e) { e.preventDefault(); return NotImplemented('RSS feeds'); });
          $('.not-implemeneted').click(function (e) { e.preventDefault(); return NotImplemented(); });
          console.log('Executed NotImplemented TimeOut');
        }, 1000);
      }
    };

    ZeroTalk.prototype.addInlineEditors = function () {
      var editor, elem, elems, i, len;
      this.logStart('Adding inline editors');
      elems = $('[data-editable]');
      for (i = 0, len = elems.length; i < len; i++) {
        elem = elems[i];
        elem = $(elem);
        if (!elem.data('editor') && !elem.hasClass('editor')) {
          editor = new InlineEditor(elem, this.getContent, this.saveContent, this.getObject);
          elem.data('editor', editor);
        }
      }
      return this.logEnd('Adding inline editors');
    };

    ZeroTalk.prototype.getContent = function (elem, raw) {
      if (raw == null) {
        raw = false;
      }
      return elem.data('content');
    };

    ZeroTalk.prototype.getObject = function (elem) {
      if (elem.data('object')) {
        return elem;
      } else {
        return elem.parents('[data-object]');
      }
    };

    ZeroTalk.prototype.saveContent = function (elem, content, cb) {
      var deleteObject, id, object, ref, type;
      if (cb == null) {
        cb = false;
      }
      if (elem.data('deletable') && content === null) {
        deleteObject = true;
      } else {
        deleteObject = false;
      }
      object = this.getObject(elem);
      ref = object.data('object').split(':');
      type = ref[0];
      id = ref[1];
      return User.getData((function (_this) {
        return function (data) {
          var comment, commentId, commentUri, ref1, ref2, ref3, ref4, topic, topicCreatorAddress, topicId, topicUri, userAddress;
          /*
            To Do:
              * What are the two variables below used for and why aren't they used?
              * Eslint gives a warning that their defined but not used is this true?
          */
          console.log(topicCreatorAddress);
          console.log(userAddress);

          if (type === 'Topic') {
            ref1 = id.split('_');
            topicId = ref1[0];
            userAddress = ref1[1];
            topicId = parseInt(topicId);
            topic = ((function () {
              var i, len, ref2, results;
              ref2 = data.topic;
              results = [];
              for (i = 0, len = ref2.length; i < len; i++) {
                topic = ref2[i];
                if (topic.topic_id === topicId) {
                  results.push(topic);
                }
              }
              return results;
            })())[0];
            if (deleteObject) {
              data.topic.splice(data.topic.indexOf(topic), 1);
            } else {
              topic[elem.data('editable')] = content;
            }
          }
          if (type === 'Comment') {
            ref2 = id.split('@');
            commentUri = ref2[0];
            topicUri = ref2[1];
            ref3 = commentUri.split('_');
            commentId = ref3[0];
            userAddress = ref3[1];
            ref4 = topicUri.split('_');
            topicId = ref4[0];
            topicCreatorAddress = ref4[1];
            commentId = parseInt(commentId);
            comment = ((function () {
              var i, len, ref5, results;
              ref5 = data.comment[topicUri];
              results = [];
              for (i = 0, len = ref5.length; i < len; i++) {
                comment = ref5[i];
                if (comment.comment_id === commentId) {
                  results.push(comment);
                }
              }
              return results;
            })())[0];
            if (deleteObject) {
              data.comment[topicUri].splice(data.comment[topicUri].indexOf(comment), 1);
            } else {
              comment[elem.data('editable')] = content;
            }
          }
          return User.publishData(data, function (res) {
            if (res) {
              if (deleteObject) {
                if (cb) {
                  cb(true);
                }
                return elem.fancySlideUp();
              } else {
                if (type === 'Topic') {
                  if ($('body').hasClass('page-main') || $('body').hasClass('page-topics')) {
                    TopicList.loadTopics('list', function () {
                      if (cb) {
                        return cb(true);
                      }
                    });
                  }
                  if ($('body').hasClass('page-topic')) {
                    TopicShow.loadTopic(function () {
                      if (cb) {
                        return cb(true);
                      }
                    });
                  }
                }
                if (type === 'Comment') {
                  return TopicShow.loadComments('normal', function () {
                    if (cb) {
                      return cb(true);
                    }
                  });
                }
              }
            } else {
              if (cb) {
                return cb(false);
              }
            }
          });
        };
      })(this));
    };

    ZeroTalk.prototype.onRequest = function (cmd, message) {
      if (cmd === 'setSiteInfo') {
        return this.actionSetSiteInfo(message);
      } else {
        return this.log('Unknown command', message);
      }
    };

    ZeroTalk.prototype.writePublish = function (innerPath, data, cb) {
      return this.cmd('fileWrite', [innerPath, data], (function (_this) {
        return function (res) {
          if (res !== 'ok') {
            _this.cmd('wrapperNotification', ['error', 'File write error: ' + res]);
            cb(false);
            return false;
          }
          return _this.cmd('sitePublish', {
            'inner_path': innerPath
          }, function (res) {
            if (res === 'ok') {
              return cb(true);
            } else {
              return cb(res);
            }
          });
        };
      })(this));
    };

    /*
        What's this doing?
        From ZeroBlog (http://127.0.0.1:43110/Blog.ZeroNetwork.bit/?Post:46:ZeroNet+site+development+tutorial+2) it looks
        like it's responding to new torrents (files) that are being uploaded.

        To Do:
            * Experiment with this.
    */
    ZeroTalk.prototype.actionSetSiteInfo = function (res) {
      var siteInfo;
      siteInfo = res.params;
      this.setSiteinfo(siteInfo);
      if (siteInfo.event && siteInfo.event[0] === 'file_done' && siteInfo.event[1].match(/.*users.*data.json$/)) {
        return RateLimit(500, (function (_this) {
          return function () {
            if ($('body').hasClass('page-topic')) {
              TopicShow.loadTopic();
              TopicShow.loadComments();
            }
            if ($('body').hasClass('page-main') || $('body').hasClass('page-topics')) {
              return TopicList.loadTopics();
            }
          };
        })(this));
      }
    };

    /*
        What's this used for? Checking a users certificate / if they're logged in?
    */
    ZeroTalk.prototype.setSiteinfo = function (siteInfo) {
      this.site_info = siteInfo;
      return User.checkCert();
    };

    /*
        What's this for? It's obviously changing the height of something but what, and why?
    */
    ZeroTalk.prototype.autoExpand = function (elem) {
      var editor;
      editor = elem[0];
      if (elem.height() > 0) {
        elem.height(1);
      }
      elem.on('input', function () {
        var minHeight, newHeight, oldHeight;
        console.log(oldHeight); // Suppress unused assignment message. What's oldHeight used for and why?
        if (editor.scrollHeight > elem.height()) {
          oldHeight = elem.height();
          elem.height(1);
          newHeight = editor.scrollHeight;
          newHeight += parseFloat(elem.css('borderTopWidth'));
          newHeight += parseFloat(elem.css('borderBottomWidth'));
          newHeight -= parseFloat(elem.css('paddingTop'));
          newHeight -= parseFloat(elem.css('paddingBottom'));
          minHeight = parseFloat(elem.css('lineHeight')) * 2;
          if (newHeight < minHeight) {
            newHeight = minHeight + 4;
          }
          return elem.height(newHeight - 4);
        }
      });
      if (elem.height() > 0) {
        return elem.trigger('input');
      } else {
        return elem.height('48px');
      }
    };

    return ZeroTalk;
  })(ZeroFrame);

  window.Page = new ZeroTalk();
}).call(this);
