/*
  This is an example of how to use scrape-torrent.js

  First parse a torrent using parse-torrent and then call scrapeTorrent with
  the parsedTorrent as an argument. The example below uses promises but you
  can just as easily use callbacks by specifying a function as the second
  argument.
*/
const Promise = require('bluebird');
const parseTorrent = require('parse-torrent');
const scrapeTorrent = Promise.promisify(require('./scrape-torrent.js'));

let torrent = parseTorrent('magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d&dn=sintel.mp4&tr=udp%3A%2F%2Fexodus.desync.com%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.internetwarriors.net%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&tr=wss%3A%2F%2Ftracker.webtorrent.io&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel-1024-surround.mp4');

console.dir(torrent);

Promise.resolve()
  .then(() => scrapeTorrent(torrent))
  .then((data) => {
    console.log('Has data');
    console.dir(data);
  })
  .catch(err => console.error(err.stack));
