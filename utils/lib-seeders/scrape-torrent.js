const Client = require('bittorrent-tracker');

module.exports = function (PARSED_TORRENT, callback) {
  const PEER_ID = new Buffer('01234567890123456789');
  const PORT = 6881;

  let opts = {
    infoHash: PARSED_TORRENT.infoHash,
    announce: PARSED_TORRENT.announce,
    peerId: PEER_ID, // hex string or Buffer
    port: PORT // torrent client port
  };

  let client = new Client(opts);

  let numberOfSeeders = 0;
  let numberOfLeechers = 0;
  let successfulScrapes = 0;
  let trackers = PARSED_TORRENT.announce;
  let callbackCalled = false;

  function callCallback (err) {
    if (typeof (trackers) !== 'undefined') {
      if (trackers.length === 0) {
        client.stop({}); // Gracefully leave the swarm
        if (!callbackCalled) {
          callbackCalled = true;

          // console.log('Average number of leechers in swarm: ' + numberOfLeechers);
          // console.log('Average number of seeders in swarm: ' + numberOfSeeders);
          if (successfulScrapes > 0) {
            return callback(undefined, {
              leechers: numberOfLeechers,
              seeders: numberOfSeeders
            });
          }
          return callback(err, undefined);
        }
      }
    }
  }

  client.on('error', function (err) {
    if (err.message.indexOf('scrape not supported') > -1) {
      console.log('Scrape is not supported!');
    } else {
      console.error(err.stack);
    }
    if (typeof (trackers) !== 'undefined' || err) {
      trackers.pop();
    }

    return callCallback(err);
  });

  client.on('warning', function (err) {
    if (typeof (trackers) !== 'undefined') {
      trackers.pop();
    }
    return callCallback(err);
  });

  // start getting peers from the tracker
  client.start({});

  // scrape
  client.scrape({});

  // console.log('Scraping torrent ' + ' with infohash ' + parsedTorrent.infoHash);
  client.on('scrape', function (data) {
    if (typeof (trackers) !== 'undefined') {
      trackers.pop();
    }
    successfulScrapes++; // Increment the number of successful scrapes

    // Gets an average parsed as an integer (you can't have half a seeder)
    numberOfSeeders = parseInt(
        (numberOfSeeders + data.complete) / (successfulScrapes), 10
    );
    numberOfLeechers = parseInt(
        (numberOfLeechers + data.incomplete) / (successfulScrapes), 10
    );

    return callCallback();
  });
};
