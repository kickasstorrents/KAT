/* eslint no-multi-str: 0 */
const Promise = require('bluebird');
const db = require('sqlite');
const parseTorrent = require('parse-torrent');
const scrapeTorrent = Promise.promisify(require('./scrape-torrent.js'));
const fs = require('fs');

const DEBUG = false;

const execAsync = Promise.promisify(require('child_process').exec);

const config = {
  db: '/home/ubuntu/zeronet/data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p/data/users/zerotalk.db',
  zeronet_home: '/home/ubuntu/zeronet',
  zite_address: '1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p',
  private_key: process.env.PRIVATE_KEY
};

if (!config.private_key) throw new Error('PrivateKeyNameError: config.private_key is undefined');

const signContentJsonCmd = (dir) => {
  return '' +
    'bash -c "cd ' + config.zeronet_home + ' && ' +
    'python2 zeronet.py siteSign --inner_path ' +
    'data/users/' + dir + '/content.json --publish ' +
    config.zite_address + ' ' + config.private_key + '"';
};

const start = new Date();
const startTime = process.hrtime();
const TIMEOUT = 500;

let data;
let i = 0;

function doParse () {
  let _i = Number(i);
  let el = data[_i];
  if (_i > data.length || !el) {
    const end = new Date() - start;
    const endTime = process.hrtime(startTime);
    console.log('Finished Scraping Torrents!');
    console.info('Execution time: %dms', end);
    console.info('Execution time (hr): %ds %dms', endTime[0], endTime[1] / 1000000);
    return;
  }
  let torrents = JSON.parse(fs.readFileSync(config.zeronet_home + '/data/' + config.zite_address + '/data/users/' + el.directory + '/data.json').toString());
  let torrent;
  if (torrents['topic'][el.topic_id]) {
    torrent = torrents['topic'][el.topic_id].magnet;
    if (!torrent) {
      torrent = torrents['topic'][el.topic_id].torrent_file;
      if (!torrent) setTimeout(doParse, 1000 + i++);
      torrent = config.zeronet_home + '/data/' + config.zite_address + '/' + torrent;
      if (!(torrent.lastIndexOf('.torrent') === torrent.length - 8)) return (function () { i++; return setTimeout(doParse, TIMEOUT); }());
      try {
        torrent = fs.readFileSync(torrent);
      } catch (err) {
        if (DEBUG) {
          console.error(err.stack);
          console.error(err.message);
        }
        if (!err.message.indexOf('no such file or directory')) {
          console.error(err.stack);
          process.exit(1);
        }
        return (function () { i++; return setTimeout(doParse, TIMEOUT); }());
      }
    }
    try {
      torrent = parseTorrent(torrent);
    } catch (err) {
      if (DEBUG) {
        console.error(err.stack);
        console.error(err.message);
      }
      return (function () { i++; return setTimeout(doParse, TIMEOUT); }());
    }
    Promise.resolve()
    .then(() => scrapeTorrent(torrent))
    .then((data) => {
      console.log(torrents['topic'][el.topic_id].title);
      console.dir(data);
      torrents['topic'][el.topic_id].seeders = data.seeders;
      torrents['topic'][el.topic_id].leechers = data.leechers;
      console.log(config.zeronet_home + '/data/' + config.zite_address + '/data/users/' + el.directory + '/data.json');
      return fs.writeFileSync(
        config.zeronet_home + '/data/' + config.zite_address + '/data/users/' + el.directory + '/data.json',
        JSON.stringify(torrents, null, 2)
      );
    })
    .catch(err => console.error(err.stack))
    .finally(() => {
      Promise.resolve()
        .then(() => execAsync(signContentJsonCmd(el.directory)))
        .then((stdout) => { console.log(stdout); })
        .catch((err) => { console.error(err.stack); });
      return (function () { i++; return setTimeout(doParse, TIMEOUT); }());
    });
  } else {
    return (function () { i++; return setTimeout(doParse, TIMEOUT); }());
  }
}

Promise.resolve()
  .then(() => db.open(config.db, { Promise }))
  .catch(err => console.error(err.stack))
  .finally(() => {
    db.all('SELECT topic_id, topic.json_id, json.directory FROM topic \
            INNER JOIN json ON topic.json_id=json.json_id ORDER BY topic.added DESC')
    .then((res) => {
      data = res;
      doParse(i);
    })
    .catch((err) => {
      console.error(err.message);
    });
  });
