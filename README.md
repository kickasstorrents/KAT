# KickAssTorrents

To Do:
    Write a nice short description about the project here...

## Project Scope

Develop a working clone of Kick Ass Torrents using [HelloZeroNet/ZeroNet](https://github.com/HelloZeroNet/ZeroNet)

<img src="http://oi65.tinypic.com/sv2las.jpg" />

### How To Help

Get in touch via [ZeroMail](http://127.0.0.1:43110/Mail.ZeroNetwork.bit/?to=kickasstorrents) if you'd like to help.

### Tasks

- [ ] Finish initial prototype based off of a clone of [ZeroTorrent](http://127.0.0.1:43110/ZeroTorrent.bit/) (IN PROGRESS)
- [X] Refine it so that it can be published (published it anyway despite it's rough edges)
- [X] Decide on a license (GPL 2/3?)
- [ ] Clean up HTML
  - [X] Use templating system of some sort to make the HTML more friendly to read and develop, perhaps use [Pug](https://github.com/pugjs/pug)?
- [ ] Clean up JavaScript
  - [ ] Familiarize oneself with the [ZeroFrame API](https://zeronet.readthedocs.io/en/latest/site_development/zeroframe_api_reference/).
  - [ ] Rewrite the JavaScript (How? Perhaps use TypeScript)
  - [ ] Test the JavaScript? Again, How? Is this possible with ZeroNet?
- [ ] Clean up CSS - Right now it's a hodge-podge of copied and pasted CSS from the KAT archives. A complete re-write would be nice if only to understand it and figure out what is and is not needed.
- [ ] Forums? (Could be implemented using ZeroBoard)
- [ ] Blogs? (Could be implemented using ZeroBlog)
- [ ] Clearnet Proxy - Do we need one / should we have one?
- [ ] Seeders / Peers info - Is it possible to poll added torrents and add peer / seeder info?
- [ ] File Info - As above, is it possible to poll torrents and add file information such as files and / or directories in a torrent and their sizes?
- [ ] Database Size? - What to do about the database size? It shouldn't be too large but overtime it will grow larger. Will this be a problem? Is there any easy way to solve this if it will be a problem? If not will it require some sort of clever multi-site architecture?
