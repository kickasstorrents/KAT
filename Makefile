# If you are using a terrible OS make it so that you can actually potentially run the makefile
ifeq ($(OS), Windows_NT)
	SHELL = bash.exe
endif

NPM=npm
HTML_MINIFIER=html-minifier
PUG=pug
JAVA=java
CLOSURE=closure-compiler-v20160713.jar # https://developers.google.com/closure/compiler/, https://github.com/google/closure-compiler
RSYNC=rsync
MKDIR=mkdir
CP=cp
MV=mv
GIT=git
ZERONET_HOME=/opt/zeronet
ZERONET=$(ZERONET_HOME)/zeronet.py
ZITE=1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p
ZITE_HOME=$(ZERONET_HOME)/data/$(ZITE)
PRIVATE_KEY=""
TARGET=Release
BRANCH=master

.PHONY: views

all: copy optimizeImages minifyCSS views minifyHTML minifyJS publish

pushCopyContentJSON:
	$(CP) -i ../content.json .

pushCommit:
	$(GIT) commit -a

pushPush:
	$(GIT) push --set-upstream origin $(BRANCH)

push: pushCopyContentJSON pushCommit pushPush

sign:
	cd $(ZERONET_HOME) && \
	./zeronet.py siteSign $(ZITE) $(PRIVATE_KEY) && \
	./zeronet.py siteSign --inner_path data/users/content.json $(ZITE) $(PRIVATE_KEY)

publishSign:
	cd $(ZERONET_HOME) && \
	./zeronet.py sitePublish $(ZITE)

publish: sign publishSign

buildDeps:
	$(NPM) install -g html-minifier
	$(NPM) install -g uglify-js
	$(NPM) install -g clean-css
	$(NPM) install -g pug-cli

testDeps:
	$(NPM) install

installAllDeps: buildDeps testDeps

copy:
	$(RSYNC) -av --progress ./content $(ZITE_HOME)
	$(RSYNC) -av --progress ./dbschema.json $(ZITE_HOME)
	$(CP) -i ./content.json $(ZITE_HOME)

views:
	$(MKDIR) -p $(ZITE_HOME)/views
	@if [[ $(TARGET) == "Release" ]] ; then \
		$(PUG) $(PWD)/views/*.pug  -o $(ZITE_HOME)/views \
	; else  \
		$(PUG) -P $(PWD)/views/*.pug -o $(ZITE_HOME)/views \
	; fi
	#$(CP) ./views/*.html $(ZITE_HOME)/views

minifyCSS:
	$(MKDIR) -p $(ZITE_HOME)/css
	@if [[ $(TARGET) == "Release" ]] ; then \
	$(HTML_MINIFIER) --collapse-whitespace \
					 --collapse-inline-tag-whitespace \
					 --minify-css \
					 --remove-comments \
					 ./css/all.css \
					 -o $(ZITE_HOME)/css/all.css \
	; fi

minifyHTML:
	@if [[ $(TARGET) == "Release" ]] ; then \
	$(HTML_MINIFIER) --collapse-whitespace \
					 --minify-js index.html \
					 --minify-css \
					 --remove-comments \
					 --input-dir $(PWD) \
					 --file-ext html \
					 --output-dir $(ZITE_HOME) \
	; fi
	@if [[ $(TARGET) == "Release" ]] ; then \
	$(HTML_MINIFIER) --collapse-whitespace \
					 --minify-js \
					 --minify-css \
					 --remove-comments \
					 --input-dir $(ZITE_HOME)/views \
					 --file-ext html \
					 --output-dir $(ZITE_HOME)/views \
	; fi
	# To Do:
	#	Remove this and replace it with something more appropriate:
	rm -rf $(ZITE_HOME)/js/jquery-ui-1.12.0.custom
	rm -rf $(ZITE_HOME)/js/lib

minifyJS:
	$(JAVA) -jar $(CLOSURE) \
		./js/lib/jquery.min.js \
		./js/lib/highlight.pack.js \
		./js/lib/jquery.cssanim.js \
		./js/lib/jquery.csslater.js \
		./js/lib/jquery.easing.js \
		./js/lib/jquery.ui.js \
		./js/lib/moment.js \
		./js/lib/marked.min.js \
		./js/Class.js \
		./js/ZeroFrame.js \
		--third_party \
		--js_output_file $(ZITE_HOME)/js/libs.bundle.js
	@if [[ $(TARGET) == "Release" ]] ; then \
	$(JAVA) -jar $(CLOSURE) \
		./js/Follow.js \
		./js/InlineEditor.js \
		./js/Menu.js \
		./js/RateLimit.js \
		./js/Text.js \
		./js/Time.js \
		./js/APITopicList.js \
		./js/TopicList.js \
		./js/TopicShow.js \
		./js/User.js \
		./js/ZeroTalk.js \
		./js/all.js \
		--js_output_file $(ZITE_HOME)/js/all.js \
		--compilation_level WHITESPACE_ONLY \
	; fi

optimizeImages:
	# To Do:
	#	* Optimise images using pngcrush etc...
	$(RSYNC) -av --progress ./img $(ZITE_HOME)

test:
	./node_modules/.bin/semistandard --verbose | ./node_modules/.bin/snazzy
