# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_GUI'] = (ENV['VAGRANT_GUI'] != '') ? ENV['VAGRANT_GUI'] : false
ENV['VAGRANT_DE'] = (ENV['VAGRANT_DE'] != '' ) ? ENV['VAGRANT_DE'] : ''

# If you want a desktop environment you can run `vagrant up` with the VAGRANT_DE
# environment variable set. For example `VAGRANT_DE=unity vagrant up`
case ENV['VAGRANT_DE']
when 'unity'
  ENV['VAGRANT_DE'] = 'ubuntu-desktop'
when 'gnome'
  ENV['VAGRANT_DE'] = 'ubuntu-gnome-desktop'
when 'kde'
  ENV['VAGRANT_DE'] = 'kubuntu-desktop'
when 'xfce'
  ENV['VAGRANT_DE'] = 'xubuntu-desktop'
when 'mate'
  ENV['VAGRANT_DE'] = 'mate-desktop-environment lxdm'
when 'mate-core'
  ENV['VAGRANT_DE'] = 'mate-desktop-environment-core lxdm'
when 'lxde'
  ENV['VAGRANT_DE'] = 'lubuntu-desktop'
when 'awesome'
  ENV['VAGRANT_DE'] = 'awesome xinit lxdm'
when 'i3'
  ENV['VAGRANT_DE'] = 'i3 xinit lxdm'
end

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/xenial64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # NOTE: Install vagrant-vbguest (https://github.com/dotless-de/vagrant-vbguest)
  # to keep VirtualBox Guest Additions up to date and use native shared folders.
  # Without this plugin we fallback to rsync which provides a significantly worse
  # user experience.
  if VagrantVbguest == nil
    config.vm.synced_folder ".", "/vagrant", type: "rsync"
  else
    config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  end

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
     vb.name = "kickasstorrents"
  #   # Display the VirtualBox GUI when booting the machine
     vb.gui = ENV['VAGRANT_GUI']
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "8024"
  #
  #
     # Customise the number of virtual CPUs
     vb.cpus = "4"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
     sudo apt-get update
     sudo apt-get install -y \
     rsync \
     curl \
     wget \
     build-essential \
     git \
     make \
     python3 python3-dev \
     python python-dev \
     msgpack-python \
     python-gevent \
     python-pip \
     vim \
     tor
     sudo pip install msgpack-python --upgrade
     sudo su root -c 'echo "ubuntu:ubuntu" | chpasswd'
     sudo su ubuntu -c 'cd $HOME && \
                    curl -O -L -s https://git.io/n-install && \
                    chmod +x n-install && \
                    N_PREFIX=$HOME/n ./n-install -q -y 6.3.1 &> /dev/null 2>&1 && \
                    sudo chown -R ubuntu:ubuntu /usr/local/n \
                    source $HOME/.bashrc;sudo $HOME/n/bin/n 6.3.1;'
     sudo chown -R ubuntu:ubuntu /usr/local/lib/node_modules
     sudo chown -R ubuntu:ubuntu /usr/local/bin
     sudo sed -i 's/.*ControlPort.*/ControlPort 9051/g' /etc/tor/torrc
     sudo usermod -a -G debian-tor $USER
     #sudo systemctl enable --now tor # Uncomment to enable tor
   SHELL
   if ENV['VAGRANT_GUI'] == 'true'
     config.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install -y #{ENV['VAGRANT_DE']}
     SHELL
   end
   config.vm.provision "shell", inline: <<-SHELL
    su ubuntu -c 'cd $HOME && git clone https://github.com/HelloZeroNet/ZeroNet.git'
    su ubuntu -c 'mkdir -p $HOME/ZeroNet/data && ln -s /vagrant $HOME/ZeroNet/data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p'
    su ubuntu -c 'echo "cd $HOME/ZeroNet/data/1LBr1dHoXvJAMjDgW8Wcg5kbCoKyJnzo6p" | tee -a $HOME/.bashrc'
   SHELL
end
